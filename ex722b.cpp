#include <cmath>
#include <stdio.h>

/*
    Written by Thomas Gadek
    Program that computes how to combine points on a graph based on 
        "The average of the distances between pairs of points, one from each of the two clusters"
    9/24/2020
    Target gcc9.3.0
*/

typedef struct 
{
    double X;
    double Y;
    int cluster;
} Coordinate;

double getDistance(Coordinate* A, Coordinate* B)
{
    double xDiff = A->X - B->X; 
    double yDiff = A->Y - B->Y; 
    return sqrt(pow(xDiff,2)+pow(yDiff,2));   
}

void clusterReset(Coordinate* coordinate, int coordinateLen)
{
    for(int i=0;i<coordinateLen;++i)
    {
        coordinate->cluster = i;
        coordinate++;
    }
}

void printCoordinates(Coordinate* coordinate, int coordinateLen)
{
    printf("Pt :  Coord  : Cluster\n");
    for(int i=0;i<coordinateLen;++i)
    {
        printf("%2d : (%2d,%2d) : %2d\n", i, (int)coordinate[i].X, (int)coordinate[i].Y, coordinate[i].cluster);
    }
}

double compareClusters(Coordinate* coordinate, int clusterA, int clusterB, int coordinateLen)
{
    int count = 0;
    double totalDistance = 0;
    for(int i=0;i<coordinateLen;++i)
    {
        for(int j=i;j<coordinateLen;++j)
        {
            if ((coordinate+i)->cluster == clusterA && (coordinate+j)->cluster == clusterB)
            {
                totalDistance += getDistance((coordinate+i), (coordinate+j));
                count++;
            }
        }
    }
    return totalDistance/(double)count;
}

void combineClusters(Coordinate* coordinate, int firstCluster, int secondCluster, int coordinateLen)
{
    for(int i=0;i<coordinateLen;++i)
    {
        if(coordinate->cluster == secondCluster)
        {
            coordinate->cluster = firstCluster;
        }
        coordinate++;
    }
}

int main()
{
    int coordinateLen = 12;
    int mergeCount = 11;
    Coordinate coordinate[coordinateLen] = {{ 2, 2},{ 3, 4},{ 5, 2},{ 4, 8},
                                            { 4,10},{ 6, 8},{ 7,10},{11, 4},
                                            {12, 3},{10, 5},{ 9, 3},{12, 6}};
    clusterReset(coordinate, coordinateLen);
    printCoordinates(coordinate, coordinateLen);

    for(int i=0;i<mergeCount;++i)
    {
        double minDistance = 1e6;
        int firstCluster, secondCluster;
        for(int clusterA=0;clusterA<coordinateLen;++clusterA)
        {
            for(int clusterB=clusterA+1;clusterB<coordinateLen;++clusterB)
            {
                double currentDistance = compareClusters(coordinate, clusterA, clusterB, coordinateLen);
                if (currentDistance < minDistance)
                {
                    minDistance = currentDistance;
                    firstCluster = clusterA;
                    secondCluster = clusterB;
                }
            }
        }
        printf("combine %d and %d: avg distance %f\n", firstCluster, secondCluster, minDistance);
        combineClusters(coordinate, firstCluster, secondCluster, coordinateLen);
        printCoordinates(coordinate, coordinateLen);
    }
}
/*
OUTPUT
    Point 0: (2,2)
    Point 1: (3,4)
    Point 2: (5,2)
    Point 3: (4,8)
    Point 4: (4,10)
    Point 5: (6,8)
    Point 6: (7,10)
    Point 7: (11,4)
    Point 8: (12,3)
    Point 9: (10,5)
    Point 10: (9,3)
    Point 11: (12,6)
    combine 7 and 8: avg distance 1.414214
    combine 3 and 4: avg distance 2.000000
    combine 7 and 9: avg distance 2.121320
    combine 0 and 1: avg distance 2.236068
    combine 5 and 6: avg distance 2.236068
    combine 7 and 10: avg distance 2.490712
    combine 3 and 5: avg distance 2.858495
    combine 0 and 2: avg distance 2.914214
    combine 7 and 11: avg distance 2.928694
    combine 0 and 3: avg distance 6.842235
    combine 0 and 7: avg distance 7.654865
*/