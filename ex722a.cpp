#include <cmath>
#include <vector>
#include <stdio.h>

/*
    Written by Thomas Gadek
    Program that computes how to combine points on a graph based on 
        "The minimum of the distances between any two points, one from each cluster"
    9/24/2020
    Target gcc9.3.0
*/

struct Coordinate
{
    double X;
    double Y;
    int cluster;
};

struct Comparison
{
    int pointA;
    int pointB;
    double distance;
};

double getDistance(Coordinate* A, Coordinate* B)
{
    double xDiff = A->X - B->X; 
    double yDiff = A->Y - B->Y; 
    return sqrt(pow(xDiff,2)+pow(yDiff,2));   
}

void printComparison(Comparison* c)
{
    printf("Combine %d and %d: Distance %f\n", c->pointA, c->pointB, c->distance);
}

int getMinimumIndex(struct Comparison c[66])
{
    
    int minIndex = 0;
    double min = c[0].distance;
    for(int n=0;n<66;++n)
    {
        if (c[n].distance < min) 
        {
            min = c[n].distance;
            minIndex = n;
        }
    }
    return minIndex;
}

bool clusterDuplicate(struct Coordinate coordinate[12], Comparison* comparison)
{
    return coordinate[comparison->pointB].cluster == coordinate[comparison->pointA].cluster;
}

void updateCluster(struct Coordinate coordinate[12], Comparison* comparison)
{
    int cluster = coordinate[comparison->pointB].cluster;
    for(int i=0;i<12;++i)
    {
        if(coordinate[i].cluster == cluster)
        {
            coordinate[i].cluster = coordinate[comparison->pointA].cluster;
        }
    }
}

int main()
{
    Coordinate coordinate[12] = {{ 2, 2,0},{ 3, 4,1},{ 5, 2,2},{ 4, 8,3},
                                 { 4,10,4},{ 6, 8,5},{ 7,10,6},{11, 4,7},
                                 {12, 3,8},{10, 5,9},{ 9, 3,10},{12, 6,11}};
    Comparison comparison[66];
    int comparisonIndex = 0;
    for(int i=0;i<12;++i)
    {
        for(int j=i+1;j<12;++j)
        {
            double currentDistance = getDistance(&coordinate[i], &coordinate[j]);
            comparison[comparisonIndex] = (Comparison){i,j,currentDistance};
            ++comparisonIndex;
        }
    }

    for(int i=0;i<12;++i)
    {
        printf("Point %d: (%d,%d)\n", i, (int)coordinate[i].X, (int)coordinate[i].Y);
    }

    for(int i=0;i<66;++i)
    {
        int result = getMinimumIndex(comparison);
        if(!clusterDuplicate(coordinate, &comparison[result]))
        {
            updateCluster(coordinate, &comparison[result]);
            printComparison(&comparison[result]);
        }
        comparison[result].distance = 500;
    }
    return 0;
}
/*
OUTPUT:
    Point 0: (2,2)
    Point 1: (3,4)
    Point 2: (5,2)
    Point 3: (4,8)
    Point 4: (4,10)
    Point 5: (6,8)
    Point 6: (7,10)
    Point 7: (11,4)
    Point 8: (12,3)
    Point 9: (10,5)
    Point 10: (9,3)
    Point 11: (12,6)
    Combine 7 and 8: Distance 1.414214
    Combine 7 and 9: Distance 1.414214
    Combine 3 and 4: Distance 2.000000
    Combine 3 and 5: Distance 2.000000
    Combine 0 and 1: Distance 2.236068
    Combine 5 and 6: Distance 2.236068
    Combine 7 and 10: Distance 2.236068
    Combine 7 and 11: Distance 2.236068
    Combine 1 and 2: Distance 2.828427
    Combine 1 and 3: Distance 4.123106
    Combine 2 and 10: Distance 4.123106
*/